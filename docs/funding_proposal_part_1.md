# Alchemy DevOps Robustification Project (part 1 of 2)

## Proposal Description

The end goal of this project is for DAOstack's alchemy server/client to have a build & deploy process that's completely automated. This will come with a few benefits:

 1. Developers who want to work on adding features to Alchemy will have a reliable environment within which to develop. Currently, getting a full-stack development environment set up requires deep knowledge of alchemy's architecture.
 2. The existing Alchemy deployment will get more robust & less likely to lose data or go down for extended periods of time. If/when something does go wrong, fixes can be deployed more quickly & reliably. It will enable the system to scale so that a surge of interest in Alchemy won't crash the site.
 3. It will be easy to deploy the Alchemy frontend to serve DAOs other than Genesis Alpha. This project will drastically lower the barrier to entry for DAO #2 to enter the playing field

Along the way, 3 important milestones need to be met:

 1. one command to build everything Alchemy needs
 2. one command to deploy alchemy in developer-mode
 3. one command to deploy alchemy to production

Because it's impossible to know whether we've built everything correctly without successfully deploying in developer-mode, we can't know whether milestone 1 has been reached until we hit milestone 2. Therefore, this proposal will fund deliverable 1 of this project, which consists of the first two milestones.

## Token Requested

50 Reputation - I'd like to use the technical experience I gain during this project to support proposals that are technically feasible & in-line with making Alchemy as useful as possible for as many people as possible.

6 ETH - to pay for my food & shelter while I work on this project

## Technical description of the project

 1. One command to build everything Alchemy needs: With a single `make` command, the contracts will be compiled, the server source code packed into a container image, and the client code properly bundled along with the contract ABIs. A proxy image will be built which will serve the client image & provide HTTPS security.

 2. One command to deploy Alchemy in developer-mode: with a single `npm run start` command, the contracts will be deployed to a local eth provider (ganache), the addresses of these deployed contracts will be properly passed to the client bundle, and the server will spin up to properly cache data & serve the client-side code.

 3. One command to deploy Alchemy to production: With a single `git push` command, the source code will be pushed to a gitlab repo where a CI pipeline is kicked off. Tests will be run and, if they all pass, this project will be deployed to a live server where the rest of the world can test it & provide feedback. The deployment will happen via containers that are orchestrated by Kubernetes providing self-healing & auto-scaling capabilities.

## About Proposer/s

My name is Bo Henderson, my background and motivations can be found in the [first proposal](https://docs.google.com/document/d/1fh8o02CFgIqd1M2kLKS0OKMpIonUJN0Nt5nVplr9rIs/edit#) I submitted asking for reputation.

See my random thoughts on [twitter](https://twitter.com/bohendo).
See my previous work on [GitLab](https://gitlab.com/bohendo).

## How can proposer guarantee results?  

I'm willing to tie my real-world name, social-media profiles & reputation (see above) to this proposal.

If the community is not satisfied with the results of this proposal, I'm at peace knowing that the resulting bad reputation will make it harder for me to use my identity as collateral when I pitch proposals in the future.

Additionally, I've split an otherwise very large project into two parts to give the community a chance to stop funding this project halfway through if the first set of results don't meet expectations.

## How does the proposal promote the DAO’s objective?

The Genesis Alpha DAO is supposed to provide an example to the rest of the world of what a successful DAO is capable of: being technically healthy & reliably providing services to it's members so that it can help them achieve their goal (in our case, growing the ecosystem around DAOstack & Alchemy)

This project will demonstrate a DAO funding an upgrade that increases it's own technical health. 

By becoming more robust, it will not only provide services to it's members more reliably, but it will enable itself to grow more quickly by making it easier for it's builder-members to add new features.

By making it easier for it's members to add features, it will become better at providing it's services, encouraging more pollinators to get involved & help it grow.

## How do we report on project’s progress

Follow development live right here on gitlab

I'd like to present a 60 second summary of the progress I've made so far during each of the weekly Pollinator meetings.

## Timeline

 - Oct 2nd: Milestone #1 should be nearing completing, probably won't have too much to report though
 - Oct 9th: Optimistic date at which deliverable 1 might be finished if things go really well
 - Oct 16th: Pessimistic date at which deliverable 1 will be finished unless things go really poorly

## Future iterations

Part 2 of 2 will involve:

 - Upgrading the build process to build server/client images that are optimized for production
 - Add a couple integration tests if they don't already exist (I didn't see any at first glance)
 - Set up a container orchestrator (Kubernetes) that would be used to deploy a self-healing & auto-scaling app to production
 - Setup a CI/CD pipeline through GitLab so that available tests are run when updates are pushed to the master branch and, if these tests pass, deploys the app to production

More details to come in the next proposal
