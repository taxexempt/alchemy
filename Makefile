
# get absolute paths to important dirs
cwd=$(shell pwd)
client=$(cwd)/modules/client
server=$(cwd)/modules/server
contracts=$(cwd)/modules/contracts
graph=$(cwd)/modules/graph
proxy=$(cwd)/modules/proxy
explorer=$(cwd)/modules/explorer

# Use the version number from package.json to tag prod docker images
version=$(shell cat package.json | grep "\"version\":" | grep -o "[0-9.]\+")

# Special variable: tells make here we should look for prerequisites
VPATH=ops:build:$(client)/build:$(contracts)/build:$(server)/build

find_options=-type f -not -path "*/node_modules/*" -not -name "*.swp"
server_src=$(shell find $(server)/common $(server)/src $(find_options))
client_src=$(shell find $(client)/src $(find_options))
contract_src=$(shell find $(contracts)/contracts $(contracts)/migrations $(find_options))
contract_ops=$(shell find $(contracts)/ops $(find_options))
graph_src=$(shell find $(graph) $(find_options))
explorer_src=$(shell find $(explorer)/src $(find_options))

app=alchemy
cacher_image=$(app)_cacher
client_image=$(app)_client
server_image=$(app)_server
truffle_image=$(app)_truffle
graph_image=$(app)_graph
proxy_image=$(app)_proxy

# Make sure these directories exist
$(shell mkdir -p build $(client)/build $(server)/build $(contracts)/build $(explorer)/build)
me=$(shell whoami)

webpack=./node_modules/.bin/webpack
graph_cli=./node_modules/.bin/graph

builder_options=--name=buidler --tty --rm
	
####################
# Begin phony rules
.PHONY: default all dev prod clean

default: prod
all: dev prod
dev: cacher-dev-image client-dev-image server-dev-image contract-artifacts
prod: proxy-image graph-image
clean:
	rm -rf build
	rm -rf $(client)/build
	rm -rf $(server)/build
	rm -rf $(contracts)/build
	rm -rf $(explorer)/build

deploy: prod
	docker tag $(proxy_image):latest $(me)/$(proxy_image):latest
	docker push $(me)/$(proxy_image):latest
	docker tag $(graph_image):latest $(me)/$(graph_image):latest
	docker push $(me)/$(graph_image):latest

####################
# Begin Real Rules

# Proxy

proxy-image: ops/proxy.dockerfile ops/proxy.conf ops/proxy.entry.sh explorer.js
	docker build --file ops/proxy.dockerfile --tag $(proxy_image):latest .
	@touch build/proxy-image

# Explorer Client

explorer.js: explorer-builder $(explorer_src) $(explorer)/ops/dockerfile
	docker run $(builder_options) --volume=$(explorer)/src:/app/src --volume=$(explorer)/public:/app/public --volume=$(explorer)/build:/app/build --entrypoint=yarn explorer_builder:dev build
	@touch build/explorer.js

explorer-builder: ops/builder.dockerfile $(explorer)/package.json
	docker build --file ops/builder.dockerfile --tag explorer_builder:dev $(explorer)
	@touch build/explorer-builder

# Build graph & graph node

graph-image: graph $(graph_src)
	docker build --file $(graph)/ops/Dockerfile --tag $(graph_image):latest $(graph)
	@touch build/graph-image

graph: graph-types
	docker run $(builder_options) --volume=$(graph)/schema.graphql:/app/schema.graphql --volume=$(graph)/subgraph.yaml:/app/subgraph.yaml --volume=$(graph)/abis:/app/abis --volume=$(graph)/types:/app/types --volume=$(graph)/mappings:/app/mappings --volume=$(graph)/dist:/app/dist --entrypoint=$(graph_cli)  graph_builder:dev build subgraph.yaml
	@touch build/graph

graph-types: graph-builder
	docker run $(builder_options) --volume=$(graph)/subgraph.yaml:/app/subgraph.yaml  --volume=$(graph)/schema.graphql:/app/schema.graphql --volume=$(graph)/mappings:/app/mappings --volume=$(graph)/abis:/app/abis --volume=$(graph)/tsconfig.json:/app/tsconfig.json --volume=$(graph)/types:/app/types --entrypoint=$(graph_cli) graph_builder:dev codegen --output-dir types subgraph.yaml
	@touch build/graph-types

graph-builder: $(graph)/package.json ops/builder.dockerfile
	docker build -f ops/builder.dockerfile -t graph_builder:dev $(graph)
	@touch build/graph-builder

# Build client

client-dev-image: client.js $(client)/ops/dev.dockerfile
	docker build --file $(client)/ops/dev.dockerfile --tag $(client_image):dev $(client)
	@touch build/client-dev-image

client.js: client-builder $(contract_src) $(client_src) $(client)/ops/webpack.dev.js
	docker run $(builder_options) --volume=$(client)/ops:/app/ops --volume=$(client)/src:/app/src --volume=$(client)/build:/app/build --volume=$(client)/tsconfig.json:/app/tsconfig.json client_builder:dev || `rm $(client)/build/client.js && exit 1`

client-builder: $(client)/package.json ops/builder.dockerfile
	docker build -f ops/builder.dockerfile -t client_builder:dev $(client)
	@touch build/client-builder

# Build cacher

cacher-dev-image: cacher.js $(client)/ops/dev.dockerfile $(client)/ops/cacher.entry.sh
	docker build --file $(client)/ops/cacher.dockerfile --tag $(cacher_image):dev $(client)
	@touch build/cacher-dev-image

cacher.js: client-builder $(client_src) $(client)/ops/cacher.webpack.js
	docker run $(builder_options) --volume=$(client)/ops:/app/ops --volume=$(client)/src:/app/src --volume=$(client)/build:/app/build --volume=$(client)/tsconfig.json:/app/tsconfig.json client_builder:dev --config ops/cacher.webpack.js || `rm $(client)/build/cacher.js && exit 1`

# Build server

server-dev-image: server.js $(server)/ops/dev.dockerfile
	docker build --file $(server)/ops/dev.dockerfile --tag $(server_image):dev $(server)
	@touch build/server-dev-image

server.js: server-builder $(server_src) $(server)/ops/webpack.dev.js
	docker run $(builder_options) --volume=$(server)/ops:/app/ops --volume=$(server)/src:/app/src --volume=$(server)/build:/app/build --volume=$(server)/common:/app/common server_builder:dev || `rm $(server)/build/server.js && exit 1`

server-builder: $(server)/package.json ops/builder.dockerfile
	docker build -f ops/builder.dockerfile -t server_builder:dev $(server)
	@touch build/server-builder

# Build blockchain/contract handlers

contract-artifacts: truffle-image
	docker run $(builder_options) --volume=$(contracts)/build:/app/build --volume=$(contracts)/contracts:/app/contracts --entrypoint=truffle $(truffle_image):dev compile
	@touch build/contract-artifacts

truffle-image: $(contract_src) $(contract_ops)
	docker build -f $(contracts)/ops/truffle.dockerfile -t $(truffle_image):dev $(contracts)
	@touch build/truffle-image
