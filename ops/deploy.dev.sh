#!/usr/bin/env bash

####################
# ENV VARS

ETH_PROVIDER="http://ganache:8545"
ETH_NETWORK_ID="4447"
ETH_MNEMONIC="candy maple cake sugar pudding cream honey rich smooth crumble sweet treat"

####################

project=alchemy
cacher_image=${project}_cacher:dev
client_image=${project}_client:dev
server_image=${project}_server:dev
truffle_image=${project}_truffle:dev
database_image=postgres:10
ganache_image=trufflesuite/ganache-cli:latest

function pull_if_unavailable {
    if [[ -z "`docker image ls | grep ${1%:*} | grep ${1#*:}`" ]]
    then
        docker pull $1
    fi
}

pull_if_unavailable $database_image
pull_if_unavailable $ganache_image

function new_secret {
    secret=$2
    if [[ -z "$secret" ]]
    then
        secret=`head -c 32 /dev/urandom | xxd -plain -c 32 | tr -d '\n\r'`
    fi
    if [[ -z "`docker secret ls -f name=$1 | grep -w $1`" ]]
    then
        id=`echo $secret | tr -d '\n\r' | docker secret create $1 -`
        echo "Created secret called $1 with id $id"
    fi
}

new_secret database_dev

mkdir -p /tmp/$project
cat - > /tmp/$project/docker-compose.yml <<EOF
version: '3.4'

secrets:
  database_dev:
    external: true

volumes:
  data:
  cache:

services:
  client:
    image: $client_image
    ports:
      - '3000:3000'
    depends_on:
      - cacher
      - server
      - database
      - ganache
    environment:
      NODE_ENV: development
      CACHE_SERVER_URL: http://localhost:3002
      API_URL: http://localhost:3001
      BASE_URL: http://localhost:3000
      arcjs_network: ganache
      arcjs_providerPort: 8545
      arcjs_providerUrl: http://ganache
      arcjs_txDepthRequiredForConfirmation: 0
    volumes:
      - `pwd`/modules/client/build:/app/build
      - `pwd`/modules/client/src:/app/src
      - `pwd`/modules/contracts/build/contracts:/app/node_modules/@daostack/arc.js/migrated_contracts

  cacher:
    image: $cacher_image
    ports:
      - '3002:3002'
    depends_on:
      - ganache
      - redis
    environment:
      NODE_ENV: development
      ETH_NETWORK_ID: $ETH_NETWORK_ID
      ETH_PROVIDER: http://ganache:8545
      ETH_MNEMONIC: $ETH_MNEMONIC
      REDIS_URL: redis://redis:6379
      arcjs_network: ganache
      arcjs_providerPort: 8545
      arcjs_providerUrl: http://ganache
      arcjs_txDepthRequiredForConfirmation: 0
    volumes:
      - cache:/app/cache
      - `pwd`/modules/client/src:/app/src
      - `pwd`/modules/client/build:/app/build
      - `pwd`/modules/contracts/build/contracts:/app/node_modules/@daostack/arc.js/migrated_contracts

  redis:
    image: redis:4-alpine

  server:
    image: $server_image
    ports:
      - '3001:3001'
    depends_on:
      - database
      - ganache
    secrets:
      - database_dev
    environment:
      NODE_ENV: development
      DB_HOST: database
      DB_PORT: 5432
      DB_NAME: $project
      DB_USERNAME: $project
      DB_PASSWORD_FILE: /run/secrets/database_dev
    volumes:
      - `pwd`/modules/server/build:/app/build
      - `pwd`/modules/server/src:/app/src
      - `pwd`/modules/server/common:/app/common

  truffle:
    image: $truffle_image
    depends_on:
      - ganache
    deploy:
      mode: global
    environment:
      ETH_NETWORK_ID: $ETH_NETWORK_ID
    volumes:
      - `pwd`/modules/contracts/contracts:/app/contracts
      - `pwd`/modules/contracts/migrations:/app/migrations
      - `pwd`/modules/contracts/test:/app/test
      - `pwd`/modules/contracts/truffle.js:/app/truffle.js
      - `pwd`/modules/contracts/build:/app/build

  ganache:
    image: trufflesuite/ganache-cli:latest
    ports:
      - '8545:8545'
    command: [
      "--mnemonic", "$ETH_MNEMONIC",
      "--networkId", "$ETH_NETWORK_ID"
    ]

  database:
    image: postgres:10
    deploy:
      mode: global
    secrets:
      - database_dev
    environment:
      POSTGRES_USER: $project
      POSTGRES_DB: $project
      POSTGRES_PASSWORD_FILE: /run/secrets/database_dev
    volumes:
      - data:/var/lib/postgresql/data
EOF

docker stack deploy -c /tmp/$project/docker-compose.yml $project
rm -rf /tmp/$project

echo -n "Waiting for the $project stack to wake up."
number_of_services=7
while true
do
    sleep 3
    if [[ "`docker container ls | grep $project | wc -l | sed 's/ //g'`" == "$number_of_services" ]]
    then
        echo " Good Morning!"
        break
    else
        echo -n "."
    fi
done

