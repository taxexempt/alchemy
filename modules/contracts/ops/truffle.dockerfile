FROM alpine:latest

RUN mkdir -p /app
WORKDIR /app

# Make sure installations are cached so they don't need to be
# re-run every time the source code changes
RUN apk add --update --no-cache bash curl entr g++ gcc git jq make nodejs python &&\
    npm install -g ganache-cli && npm install -g truffle

# installing node_modules takes a long time, make sure this
# happens early enough to get cached
COPY package.json ./
RUN npm install

# Now that the longest running steps are done,
# we can add start doing stuff that'll change more frequently
COPY ops/truffle.entry.sh entry.sh
RUN chmod +x entry.sh

COPY contracts contracts
COPY migrations migrations
COPY test test
COPY truffle.js truffle.js

ENTRYPOINT ["./entry.sh"]
