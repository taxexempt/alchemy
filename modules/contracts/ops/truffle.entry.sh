#!/bin/bash

project=alchemy

# Wait for dependencies to come alive
echo -n "Waiting for eth provider (net id: $ETH_NETWORK_ID) to wake up."
while true
do
  ping -c1 -w1 ${project}_ganache > /dev/null 2> /dev/null
  if [ "$?" != "0" ]
  then
    echo -n "."
    sleep 5
    continue
  fi  
  echo " The eth provider's awake, let's go!"
  break
done

truffle compile
truffle migrate --reset --network=docker

# Watch contract code, recompile & re-migrate on changes
old_hash="`find contracts migrations -type f -not -name "*.swp" | xargs cat | sha256sum`"
echo "Watching hash for changes: $old_hash"
while true;
do
    new_hash="`find contracts migrations -type f -not -name "*.swp" | xargs cat | sha256sum`"
    if [[ "$new_hash" == "$old_hash" ]]
    then
        sleep 2;
    else
        echo 
        echo "Changes detected! New hash: $new_hash"
        echo "Re-migrating..."
        truffle migrate --reset --network=docker
        old_hash=$new_hash
    fi
done
