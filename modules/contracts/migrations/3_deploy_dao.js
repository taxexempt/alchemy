//this migration file is used only for testing purpose
var AbsoluteVote = artifacts.require('./AbsoluteVote.sol');
var Avatar = artifacts.require('./Avatar.sol');
var ContributionReward = artifacts.require('./ContributionReward.sol');
var ControllerCreator = artifacts.require('./ControllerCreator.sol');
var DaoCreator = artifacts.require('./DaoCreator.sol');
var DaoToken = artifacts.require('./DaoToken.sol');
var GenesisProtocol = artifacts.require('./GenesisProtocol.sol');
var GlobalConstraintRegistrar = artifacts.require('./GlobalConstraintRegistrar.sol');
var QuorumVote = artifacts.require('./QuorumVote.sol');
var SchemeRegistrar = artifacts.require('./SchemeRegistrar.sol');
var SimpleICO = artifacts.require('./SimpleICO.sol');
var TokenCapGC = artifacts.require('./TokenCapGC.sol');
var UpgradeScheme = artifacts.require('./UpgradeScheme.sol');
var VestingScheme = artifacts.require('./VestingScheme.sol');
var VoteInOrganizationScheme = artifacts.require('./VoteInOrganizationScheme.sol');
var OrganizationRegister = artifacts.require('./OrganizationRegister.sol');
var Redeemer = artifacts.require('./Redeemer.sol');
var UController = artifacts.require('./UController.sol');

// Stupid truffle using stupid old version of web3
const toWei = web3.utils ? web3.utils.toWei : web3.toWei;

// TEST_ORGANIZATION ORG parameters:
const orgName = "Genesis Alpha";
const tokenName = "Genesis Alpha";
const tokenSymbol = "GDT";
const nFounders = 5;
const founders = []; // will be filled in during migration
const initRep = Array(nFounders).fill(toWei("1000"))
const initToken = Array(nFounders).fill(toWei("1000"))
const cap = toWei("100000000","ether");
const options = { gas: 6100000 };

module.exports = function (deployer, network, accounts) {
  return (deployer.then(async function() {

    // Get instances of universal contracts
    var uControllerInst = await UController.deployed()
    var daoCreatorInst = await DaoCreator.deployed();

    // construct array of founders
    for (let i=0; i<5; i++) { founders[i] = accounts[i]; }

    // log the params used to create this DAO
    console.log(`  DaoCreator (${daoCreatorInst.address}) forging: ${JSON.stringify({
      orgName: orgName,
      tokenName: tokenName,
      tokenSymbol: tokenSymbol,
      founders: founders,
      initToken: initToken,
      initRep: initRep,
      uController: uControllerInst.address,
      cap: cap
    }, null, 2)}`)

    var returnedParams = await daoCreatorInst.forgeOrg(
      orgName,
      tokenName,
      tokenSymbol,
      founders,
      initToken,
      initRep,
      uControllerInst.address,
      cap,
      options
    );

    var AvatarInst = await Avatar.at(returnedParams.logs[0].args._avatar);
    console.log(`  Avatar: ${AvatarInst.address}`)




  }));
};
