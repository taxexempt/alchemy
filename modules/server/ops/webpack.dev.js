const path = require('path');
const nodeExternals = require('webpack-node-externals');

module.exports = {
  mode: "development",
  target: 'node',
  externals: [nodeExternals({ modulesFromFile: true })],

  entry: {
      server: './src/server.js',
  },

  output: {
    path: path.join(__dirname, '../build'),
    filename: '[name].js',
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/env'],
          },
        },
      },
    ],
  },

  stats: {
    warnings: false,
  },
};
