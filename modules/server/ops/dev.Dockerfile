FROM server_builder:dev

COPY build build
COPY src src
COPY common common

EXPOSE 3001
ENTRYPOINT ["nodemon"]
CMD [ "--legacy-watch", "--exitcrash", "--watch", "build/server.js", "build/server.js" ]
