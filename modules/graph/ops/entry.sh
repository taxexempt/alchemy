#!/bin/bash

echo -n "Waiting for ipfs to wake up."
while true
do
  ping -c1 -w1 ipfs > /dev/null 2> /dev/null
  if [ "$?" != "0" ]
  then
    echo -n "."
    sleep 5
    continue
  fi  
  echo " ipfs is awake, let's go!"
  break
done

output=`./node_modules/.bin/graph build --ipfs /dns4/ipfs/tcp/5001 subgraph.yaml`
hash=`echo $output | grep "Subgraph:" | awk '{print$2}'`

echo "Subgraph: $hash"

exec graph-node \
  --postgres-url "postgresql://$postgres_user:$postgres_pass@$postgres_host/$postgres_db" \
  --ethereum-ws "$ethereum" \
  --ipfs "$ipfs" \
  --subgraph "$hash"
