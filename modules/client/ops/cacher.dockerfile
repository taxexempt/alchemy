FROM client_builder:dev

COPY ops ops
COPY src src
COPY build build
COPY tsconfig.json tsconfig.json

ENTRYPOINT ["bash"]
CMD ["ops/cacher.entry.sh"]
