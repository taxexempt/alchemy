#!/bin/bash

webpack --watch --config ops/cacher.webpack.js &

exec nodemon --legacy-watch --exitcrash --watch build --watch node_modules/@daostack/arc.js/migrated_contracts build/cacher.js
