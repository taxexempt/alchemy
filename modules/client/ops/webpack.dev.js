const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const baseConfig = require('./webpack.base.js');

// tslint:disable-next-line:no-var-requires
require('dotenv').config();

module.exports = merge(baseConfig, {
  entry: [
    // activate HMR for React
    'react-hot-loader/patch',
    // bundle the client for webpack-dev-server
    // and connect to the provided endpoint
    'webpack-dev-server/client?http://localhost:3000',
    // bundle the client for hot reloading
    // only- means to only hot reload for successful updates
    'webpack/hot/only-dev-server',
    // the entry point of our app
    __dirname + '/../src/index.tsx',
  ],

  output: {
    filename: "client.js",
    path: path.resolve(__dirname, '../build'),
    // necessary for HMR to know where to load the hot update chunks
    publicPath: '/'
  },

  mode: 'development',
  devtool: 'cheap-module-eval-source-map',
  devServer: {
    host: '0.0.0.0',
    port: 3000,
    hot: true,
    contentBase: path.resolve(__dirname, '../build'),
    publicPath: '/',
    historyApiFallback: true,
    watchOptions: {
      poll: 1000,
      ignored: /node_modules\/[^@]/,
    },
  },

  stats: {
    warnings: false,
  },

  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [
          { // creates style nodes from JS strings
            loader: "style-loader"
          },
          { // translates CSS into CommonJS (css-loader) and automatically generates TypeScript types
            loader: 'typings-for-css-modules-loader',
            options: {
              camelCase: true,
              modules: true,
              namedExport: true,
              localIdentName: '[name]__[local]___[hash:base64:5]',
              importLoaders: 2,
              sourceMap: true
            }
          },
          { // compiles Sass to CSS
            loader: "sass-loader",
            options: {
              sourceMap: true
            }
          },
          { // Load global scss files in every other scss file without an @import needed
            loader: 'sass-resources-loader',
            options: {
              resources: ['./src/assets/styles/global-variables.scss']
            },
          },
        ]
      }
    ],
  },

  plugins: [
    // enable HMR globally
    new webpack.HotModuleReplacementPlugin(),
    // Prints more readable module names in the browser console on HMR updates
    new webpack.NamedModulesPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify(process.env.jj),
        'CACHE_SERVER_URL': JSON.stringify(process.env.CACHE_SERVER_URL),
        'API_URL': JSON.stringify(process.env.API_URL),
        'BASE_URL': JSON.stringify(process.env.BASE_URL),
        'arcjs_network': JSON.stringify(process.env.arcjs_network),
        'arcjs_providerPort': JSON.stringify(process.env.arcjs_providerPort),
        'arcjs_providerUrl': JSON.stringify(process.env.arcjs_providerUrl),
        'arcjs_txDepthRequiredForConfirmation': JSON.stringify(process.env.arcjs_txDepthRequiredForConfirmation),
      }
    })
  ]
});
