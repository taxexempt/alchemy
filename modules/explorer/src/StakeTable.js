import React from 'react';

class StakeTable extends React.Component {
    render() {
        const { stake, viewProposal, alchemyData } = this.props
        if (!stake) return null
        return (
            <div>
                    <div key={stake.proposal.id} className="row">
                        <div className="col-7">
                            <a href={alchemyData[stake.proposal.id].description}>{alchemyData[stake.proposal.id].title}</a>
                        </div>
                        <div className="col-3">
                            {stake.stake}
                        </div>
                        <div className="col-1">
                            {stake.vote === "1" ? <p> For </p> : <p> Against </p>}
                        </div>
                        <div className="col-1">
                            <button onClick={() => viewProposal(stake.proposal.id)}>
                                Info
                            </button>
                        </div>
                    </div>
            </div>
        )
    }
}

export { StakeTable }
