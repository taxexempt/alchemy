import React from 'react';

class StakerTable extends React.Component {
    render() {
        const { stake, viewUser, alchemyData } = this.props
        if (!stake) return null
        return (
            <div>
                    <div key={stake.staker.id} className="row">
                        <div className="col-7">
                            {stake.staker.id}
                        </div>
                        <div className="col-3">
                            {stake.stake}
                        </div>
                        <div className="col-1">
                            {stake.vote === "1" ? <p> For </p> : <p> Against </p>}
                        </div>
                        <div className="col-1">
                            <button onClick={() => viewUser(stake.staker.id)}>
                                Info
                            </button>
                        </div>
                    </div>
            </div>
        )
    }
}

export { StakerTable }
