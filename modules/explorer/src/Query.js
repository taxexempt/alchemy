let stakes = `stakes {
    staker{id}
    stake
    vote
}`

let votes = `votes {
    voter{id}
    vote
}`

let rewards = `rewards {
    type
    beneficiary{id}
    amount
}`

let common = `
    id
    proposer{id}
    avatar
    state
    decision
`
const q = {}

q.getAllProposals = () => {
    return `
        query {
            proposals { 
                id
                decision
            }
        }
    `
}

q.getAllProposalsWithDetails = () => {
    return `
        query {
            proposals {
                ${common}
                ${stakes}
                ${votes}
                ${rewards}
            }
        }
    `
}

q.getAllProposalsFor = (avatar) => {
    return `
        query {
            proposals (
                where: {
                    avatar_in: ["${avatar}"]
                }
            ) {
                id
                decision
            }
        }
    `
}

q.getAllProposalsWithDetailsFor = (avatar) => {
    return `
        query {
            proposals (
                where: {
                    avatar_in: ["${avatar}"]
                }
            ) {
                ${common}
                ${stakes}
                ${votes}
                ${rewards}
            }
        }
    `
}

q.getAllProposalsProposedBy = (proposer) => {
    return `
        query {
            proposals (
                where: {
                    proposer_in: ["${proposer}"]
                }
            ) {
                ${common}
                ${stakes}
                ${votes}
                ${rewards}
            }
        }
    `
}

q.getAllProposalsVotedBy = (voter) => {
    return `
        query {
            votes{
                where: {
                    voter_in: ["${voter}"]
                }
                vote
                proposal {
                    ${common}
                }
            }
        }
    `
}

q.getAllProposalsStakedBy = (staker) => {
    return `
        query {
            stakes{
                where: {
                    staker_in: ["${staker}"]
                }
                vote
                stake
                proposal {
                    ${common}
                }
            }
        }
    `
}

q.getDetailsOfProposal = (id) => {
    return `
        query {
            proposal(id: "${id}") {
                ${common}
                ${stakes}
                ${votes}
                ${rewards}
            }
        }
    `
}

q.getAllRewardsFor = (beneficiary) => {
    return `
        query {
            rewards (
                where: {
                    beneficiary_in: ["${beneficiary}"]
                }
            ) {
                ${rewards}
            }
        }
    `
}

q.getAllUsers = () => {
    return `
        query {
            users {id}
        }
    `
}

q.getAllDetailsForUser = (user) => {
    return `
        query {
            user (id: "${user}") {
                id
                proposals {
                    id
                    decision
                }
                stakes {
                    stake
                    vote
                    proposal {id}
                }
                votes {
                    vote
                    proposal {id}
                }
            }
        }
    `
}

export default q
