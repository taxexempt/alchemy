import React, { Component } from 'react';
import axios from 'axios';
import _ from 'lodash';
import './App.css';
import q from './Query';
import { ProposalTable } from './ProposalTable';
import { UserTable } from './UserTable';
import { ProposalDetails } from './ProposalDetails';
import { UserDetails } from './UserDetails';

const url = 'http://localhost:8000/cli/graphql';
const alchemy = 'https://daostack-alchemy.herokuapp.com/api/proposals';

class App extends Component {
    constructor(props) {
        super(props)
        this.display = this.display.bind(this)
        this.viewProposal = this.viewProposal.bind(this)
        this.viewUser = this.viewUser.bind(this)
        this.state = {
            error: null,
            proposals: null,
            users: null,
            proposal: null,
            user: null,
            type: "",
            mode: "Welcome",
            alchemyData: null
        }
    }

    async viewProposal(id) {
        const result = await this.execute(q.getDetailsOfProposal(id));
        //console.log(JSON.stringify(result, null, 2))
        this.setState({
            mode: "ProposalDetails",
            proposal: result.proposal
        })
    }

    async viewUser(user) {

        //console.log(JSON.stringify(user, null, 2));
        const result = await this.execute(q.getAllDetailsForUser(user), {});
        this.setState({
            mode: "UserDetails",
            user: result.user
        })
    }

    async execute (query, variables) {
        try{
            const response = await axios.post(url,
                {
                  query,
                  variables,
                  crossdomain: true,
                  headers: {
                    'Access-Control-Allow-Origin': '*',
                    'Content-Type': 'application/json',
                  }
                },
            );

            //console.log(JSON.stringify(response, null, 2));
            
            return response.data.data;
        } catch (error) { this.setState(() => ({ error })) }
    }

    async viewProposals() {
        if (this.state.proposals) {
            this.setState({ mode : "Proposals" })
            return null;
        }

        const result = await this.execute(q.getAllProposalsFor("0xa3f5411cfc9eee0dd108bf0d07433b6dd99037f1"), {});

        this.setState(() => ({
            mode: "Proposals",
            proposals: result.proposals
        }));
    }

    async viewUsers() {
        if (this.state.users) {
            this.setState({ mode : "Users" })
            return null;
        }
        const result = await this.execute(q.getAllUsers(), {});
        //console.log(JSON.stringify(result, null, 2));

        this.setState(() => ({
            mode: "Users",
            users: result.users
        }));
    }

    async componentDidMount() {
        try {
            const result = await axios.get(alchemy);
            this.setState(() => ({
                alchemyData: _.keyBy(result.data, "arcId")
            }))
            //console.log(JSON.stringify(this.state.alchemyData, null, 2))
        } catch (error) {
            this.setState(() => ({error}))}
    }

    display() {

        if (this.state.mode  === "Proposals") {
            return <ProposalTable rowdata={this.state.proposals} viewProposal={this.viewProposal} alchemyData={this.state.alchemyData} />
        } else if (this.state.mode  === "Users") {
            return <UserTable rowdata={this.state.users} viewUser={this.viewUser} alchemyData={this.state.alchemyData} />
        } else if (this.state.mode === "ProposalDetails") {
            return <ProposalDetails proposal={this.state.proposal} viewProposal={this.viewProposal} viewUser={this.viewUser} alchemyData={this.state.alchemyData} />
        } else if (this.state.mode === "UserDetails") {
            return <UserDetails user={this.state.user} viewProposal={this.viewProposal} viewUser={this.viewUser} alchemyData={this.state.alchemyData} />
        }
    }

    render() {
        return (
            <div className="App">
                <button onClick={() => this.viewProposals()}>
                    Explore Proposals
                </button>
                
                <button onClick={() => this.viewUsers()}>
                    Explore Users
                </button>
                
                { this.state.mode === "Welcome" ?  <h2>Make your selection</h2> : this.display()}

            </div>
        );
    }
}


export default App;
