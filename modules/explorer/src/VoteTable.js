import React from 'react';

class VoteTable extends React.Component {
    render() {
        const { vote, viewProposal, alchemyData } = this.props
        console.log(JSON.stringify(vote, null, 2))
        if (!vote) return null
        return (
            <div>
                <div key={vote.proposal.id} className="row">
                    <div className="col-7">
                        <a href={alchemyData[vote.proposal.id].description}>{alchemyData[vote.proposal.id].title}</a>
                    </div>
                    <div className="col-3">
                        {vote.vote === "1" ? <p> For </p> : <p> Against </p>}
                    </div>
                    <div className="col-2">
                        <button onClick={() => viewProposal(vote.proposal.id)}>
                            Info
                        </button>
                    </div>
                </div>
            </div>
        )
    }
}

export { VoteTable }
