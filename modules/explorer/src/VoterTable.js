import React from 'react';

class VoterTable extends React.Component {
    render() {
        const { vote, viewUser, alchemyData } = this.props
        console.log(JSON.stringify(vote, null, 2))
        if (!vote) return null
        return (
            <div>
                <div key={vote.voter.id} className="row">
                    <div className="col-7">
                        {vote.voter.id}
                    </div>
                    <div className="col-3">
                        {vote.vote === "1" ? <p> For </p> : <p> Against </p>}
                    </div>
                    <div className="col-2">
                        <button onClick={() => viewUser(vote.voter.id)}>
                            Info
                        </button>
                    </div>
                </div>
            </div>
        )
    }
}

export { VoterTable }
