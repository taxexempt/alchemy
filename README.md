# Alchemy

## Overview

This is a monorepo that combines the power of:

 - [DAOstack's arc contracts](https://github.com/daostack/arc)
 - [The alchemy node server](https://github.com/daostack/daostack-node)
 - [The alchemy client](https://github.com/daostack/daostack)

And, eventually, I'd like to integrate the [DAOstack graph protocol subgraph](https://gitlab.com/shivgupt/DAOstack-subgraph) under development by @shivgupt

# Funding

This project will hopefully be funded by the Genesis Alpha DAO itself, see `docs/funding_proposal_part_1.md` for details.

# Motivation

TL;DR the goal is for DAOstack's alchemy project to be easily portable to DAOs other than Genesis Alpha simply by providing a new avatar address as a parameter & running a couple commands.

My team & I were inspired by DAOstack's platform and decided to build something with it at EthBerlin. Seeing that Alchemy is the DAOstack frontend with the most features & a beautiful UI, we came into the hackathon with the goal of cloning the genesis DAO and adding a couple interesting features to it. Unfortunately, we spent several hours wrestling with the Alchemy repos and were not able to get even the existing project deployed let alone with any added features.

Fear not, we pivoted to using the [DAOstack hackers kit](https://github.com/daostack/DAOstack-Hackers-Kit) and were able to use it to develop a [debtor DAO](https://gitlab.com/bohendo/debtor-dao) that was worthy of recognition by [DAOstack](https://medium.com/daostack/daostack-update-hackers-kit-is-live-introducing-our-bounty-winners-from-ethberlin-genesis-f92524492e55) and [Dharma](https://blog.dharma.io/dharma-community-update-17-sept-2018-33ceb2b9509b).. Although our end product would have been much more beautiful & featureful if we'd have been able to leverage Alchemy.

The goal with this project is to make it as easy as possible for future hackers to build on top of Alchemy. This goal has a few important components:

 1. one-command build: with a single `make` command, the contracts should be compiled, the server source code bundled & packed into a container image, and the client code properly bundled along with the contract ABIs.

 2. one-command dev-mode deployment: with a single `npm run start` command, the contracts should be deployed to a local eth provider, the addresses of these deployed contracts should be properly passed to the client-side code, and the server should spin up to properly cache data & serve the client bundle.

 3. one-command production deployment: with a single `git push` command, the source code should be pushed to a gitlab repo where a CI pipeline is kicked off. Tests will be run and, if they all pass, this project should be deployed to a live server where the rest of the world can test it & provide feedback. The deployment should be via containers that are orchestrated by Kubernetes providing self-healing & auto-scaling capabilities.

# Contract Cheatsheet

### [DAOToken](https://etherscan.io/address/0x543Ff227F64Aa17eA132Bf9886cAb5DB55DCAddf)
(Global)
GEN Token
Registered as the staking token in the GenesisProtocol contract.

### [UController](https://etherscan.io/address/0xa50c298118e89e0cc465f2a1705c7579f67b6f26)
(Global)
Owner of the Alchemy avatar and many others. The Avatar is registered here along with it's Reputation & Token addresses.
This contract manages which schemes & global constraints are registered with avatars.

### [GenesisProtocol](https://etherscan.io/address/0x8940442e7f54e875c8c1c80213a4aee7eee4781c)
(Global)
Voting machine that powers Alchemy.
Contains staking logic for boosting proposals and voting logic for passing proposals (binary yes/no outcomes).
When a proposal passes, the scheme that was being voted on gets executed. In Alchemy's case, this scheme is ContributionReward.
(part of infra, not arc)

### [ContributionReward](https://etherscan.io/address/0xc282f494a0619592a2410166dcc749155804f548)
(Global)
A scheme registered to Alchemy, it allows members to propose paying some amount of Eth or ERC20 to some beneficiary address

### [Redeemer](https://etherscan.io/address/0x67b13f159ca093325554aac6ee104fce36f3f9dd)
(Global)
Helper contract that lets you redeem your GEN from staking, Reputation for voting, and your contribution reward if any all in one clean transaction.
Linked to ContributionReward and GenesisProtocol.

### [Avatar](https://etherscan.io/address/0xa3f5411cfc9eee0dd108bf0d07433b6dd99037f1)
(Local)
The address of your DAO according to the rest of the ecosystem. The avatar's balance is your DAOs balance. It is controlled by the UController. It is tied to a native reputation and token but I don't think either of these are significant..

**Alchemy Schemes**:
Search [UController events](https://etherscan.io/address/0xa50c298118e89e0cc465f2a1705c7579f67b6f26#events) with this topic hash:
`keccak('RegisterScheme(address,address,address)') = 0x521230fe5fa463e77907aa7d9653b8cf93661e82561e966af8aa0a99910554c1`

Schemes registered to the Alchemy Avatar:
 - [Owner](https://etherscan.io/address/0x87ac6d6874c998f0f645f7db76b500d265c1856d#code)
 - [SchemeRegistrar](https://etherscan.io/address/0xf7122bb9f34c1ffbdc961940ed6aa6000fbf3ec7#code)
 - [GlobalConstraintRegistrar](https://etherscan.io/address/0xe000e03d1f4eaa3f7eb33d3473e720ca1b44f145#code)
 - [UpgradeScheme](https://etherscan.io/address/0xd96e541d8559a45bb226dc5488fd38bdc15e9c84#code)
 - [ContributionReward](https://etherscan.io/address/0xc282f494a0619592a2410166dcc749155804f548#code)
 - [GenesisProtocol](https://etherscan.io/address/0x8940442e7f54e875c8c1c80213a4aee7eee4781c#code)
 - [MultiSigWallet](https://etherscan.io/address/0x5e682cb9b6dc4b0fd299908672839556485e992b#code)
 - [GlobalConstraintRegistrar](https://etherscan.io/address/0xe000e03d1f4eaa3f7eb33d3473e720ca1b44f145#code)

### [Reputation](https://etherscan.io/address/0x13ad61aa8f695ce64711a51a6feccb48a275aaf8)
(Local)
Alchemy's native reputation contract. This contract is a bit like an ERC20 contract minus any transfer methods.
It holds reputation balances & this address registered in the UController's reputations mapping.

### [Token](https://etherscan.io/address/0x1adb85aa742114815928a813bde3968614ea58fe)
(Local)
Alchemy's native token. Doesn't seem to be used for much.
Only one [mysterious transaction](https://etherscan.io/tx/0xf432356429da3efbabd08ecff4a4a7ff5a2c1bad889d8743e5ffd90f71628aee) seems to have interacted with these tokens.

### ContractCreator
(Global)
Helper contract for deploying new controllers

### DaoCreator
(Global)
Helper contract for deploying new organizations.
It can
 - deploy a new Avatar
 - link it to a controller
 - register schemes
 - mint native tokens & reputation for founders
